const dotenv = require("dotenv");

dotenv.config();

const config = {
  MONGOURI: process.env.CLUSTER_URL,
  JWT_SECRET: process.env.JWT_SECRET,
};

module.exports = config;
